## Installation du NAS

L'installation d'un NAS Synology est relativement simple, il suffit de se rendre sur le [Centre de téléchargements de Synology](https://www.synology.com/fr-fr/support/download) et de télécharger **DSM** (le système d'exploitation du NAS) et **Synology Assistant** qui va permettre de détecter et d'installer DSM.

#### Synology Assistant

Lancement de Synology Assistant.

![Synology Assistant](../img/0001.png)

---

Allez cherchez DSM, le fichier **.pat** précédemment télécharger depuis le [Centre de téléchargements de Synology](https://www.synology.com/fr-fr/support/download)

![Synology Assistant](../img/0002.png)

---

Le compte Administrateur par défaut est `admin` choisissez un password, ce compte sera désactiver par la suite pour des raisons de sécurité

![Synology Assistant](../img/0003.png)

---

Après quelques minutes l'installation est fini, cliquez sur `Se connecter à DiskStation`

![Synology Assistant](../img/0004.png)


#### DSM

![DSM](../img/0005.png)

---

Il est préférable d'installer automatiquement les mise à jour du système.

![DSM](../img/0006.png)

---

Pour info

![DSM](../img/0008.png)

![DSM](../img/0007.png)

---

J'ai choisie d'omettre cette étape je préfère les installer par la suite.

![DSM](../img/0009.png)

---

C'est prêt !

![DSM](../img/0010.png)
