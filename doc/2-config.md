## Configuration de DSM

#### Désactivation de l'hibernation des disques

![hibernation](../img/0011.png)

Il est recommandé de désactiver l'hibernation des disques.

---

#### Changement du compte Administrateur

![IMG](../img/0012.png)

![IMG](../img/0013.png)

![IMG](../img/0014.png)

![IMG](../img/0015.png)

![IMG](../img/0016.png)

![IMG](../img/0017.png)

![IMG](../img/0018.png)

![IMG](../img/0019.png)

Pour désactiver le compte admin par défaut fermer votre session et logger vous avec votre nouveau compte admin (`MonID` pour ma part)

![IMG](../img/0020.png)

![IMG](../img/0021.png)

---

#### Activez la double vérification

[C'est quoi la double authentification ?](http://www.lesnumeriques.com/vie-du-net/comment-activer-double-authentification-sur-comptes-web-a2575.html)

[Quel service l'utilise ?](https://twofactorauth.org)

* Application Mobile :

	* Android : TODO

	* iOS : TODO

	* BlackBerry : TODO

	* Windows Mobile : TODO

* Application Desktop

	* [Keepass](http://keepass.info)



Accessible depuis le menu perso

![IMG](../img/0022.png)

![IMG](../img/0023.png)

Et dans le panneau de configuration

![IMG](../img/0024.png)

![IMG](../img/0025.png)



![IMG](../img/0026.png)

![IMG](../img/0027.png)

Prochain login

![IMG](../img/0028.png)

---

#### Politique des mots de passe et service d'accueil utilisateur

![IMG](../img/0029.png)

---

#### Changer la page de login

![IMG](../img/0030.png)

Avant
![IMG](../img/0031.png)

Après
![IMG](../img/0032.png)
