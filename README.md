> Guide sans prétention pour s'auto héberger avec un NAS Synology.

## Table of Contents
1. [Installation du NAS](doc/1-installation.md)
    1. [Synology Assistant](doc/1-installation.md#synology-assistant)
    2. [DSM](doc/1-installation.md#dsm)
2. [Configuration de base](doc/2-config.md)
    1. Désactivation de l'hibernation des disques
    2. Changement du compte Administrateur
    2. Activez la double vérification
    3. Politique des mots de passe et service d'accueil utilisateur
    4. Changer la page de login
3. Applications Synology
	1. [Note](app/note.md)

### TODO
- [X] Installation
- [ ] Gestion du nom de domaine
- [ ] Mail
- [ ] Note
- [ ] Contacts (Carddav)
- [ ] Calendriers (Caldav)
- [ ] Musique
- [ ] Backup
- [ ] Sécurité
- [ ] Downloads



### Liens Web

[Synology](www.synology.com)

[Centre de téléchargements](https://www.synology.com/fr-fr/support/download)

[Synology Web Assistant](http://find.synology.com/)
